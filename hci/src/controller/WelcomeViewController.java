package controller;

import controller.base.BaseController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

/**
 * Created by Dana on 25-Nov-17.
 */
public class WelcomeViewController extends BaseController {
    @FXML
    Button start_button;

    @FXML
    private void initialize() {
    }

    @FXML
    public void handleStartButton(ActionEvent e) {
        System.out.println("Iuhuuu");
        showOkMessage();
    }
}