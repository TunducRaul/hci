package controller.base;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

public abstract class BaseController {
    private Scene baseScene;
    private Stage baseStage;

    private static String OK_MESSAGE = "Bravo!";
    private static String ERROR_MESSAGE = "Mai încearcă!";

    public Scene getBaseScene() {
        return baseScene;
    }

    public void setBaseScene(Scene baseScene) {
        this.baseScene = baseScene;
    }

    public Stage getBaseStage() {
        return baseStage;
    }

    public void setBaseStage(Stage baseStage) {
        this.baseStage = baseStage;
    }

    protected void showOkMessage() {
        showMessage(Alert.AlertType.INFORMATION, OK_MESSAGE);
    }

    protected void showErrorMessage() {
        showMessage(Alert.AlertType.INFORMATION, ERROR_MESSAGE);
    }

    private void showMessage(Alert.AlertType type, String text) {
        //Creare si setare textBox pentru alerta
        try {
            Stage stage = new Stage();
            stage.initOwner(baseStage);
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ClassLoader.getSystemResource("view/ok_message_view.fxml"));
            Parent root = loader.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}